﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Assignment_1
{
    public partial class HotelBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void Book(object sender, EventArgs e)
        {
            orderDetails.Text = "Order Details";
            Client client = new Client();
            client.Title = title.Text;
            client.ClientFName = clientFirstName.Text;
            client.ClientLName = clientLastName.Text;
            client.ClientEmail = clientEmail.Text;
            client.ClientContact = clientContact.Text;

            List<string> foodFacility = new List<string>();

            foreach(Control control in foodFacilityCheckbox.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox foodfacility1 = (CheckBox)control;
                    if(foodfacility1.Checked)
                    {
                        foodFacility.Add(foodfacility1.Text);
                    }
                }
            }

            int stay = Int32.Parse(stayDays.Text);

            //NOT ABLE TO DO OPERATION WITH DATE SO NEEDED TO TAKE NO OF DAYS AS WELL
            //DateTime deliverytime = DateTime.ParseExact(trydate, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
          //  DateTime start = DateTime.ParseExact(startDate.Text, "dd-mm-yyyy", CultureInfo.InvariantCulture);
           // DateTime end = DateTime.ParseExact(endDate.Text, "dd-mm-yyyy", CultureInfo.InvariantCulture);
            Room room = new Room(suiteType.Text,foodFacility,startDate.Text,endDate.Text,stay );

            Payment payment = new Payment();
            payment.PaymentMode = paymentMode.Text;
            Booking booking = new Booking(client,room,payment);

            bookingDetails.InnerHtml = booking.BookingDetails();//TO PRINT BOOKING DETAILS

           /* DateTime sDate =DateTime.Parse(startDate.Text);
            DateTime eDate = DateTime.Parse(endDate.Text);
            //sDate = DateTime.Parse(sDate);
            bookingDetails.InnerHtml = sDate - eDate;
            */
        }
    }
}