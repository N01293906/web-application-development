﻿<!--
    Name:Yash Gandhi
    Student No: N01293906
    Date: 20/09/2018
    course: Web Application Development
    -->
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="Assignment_1.HotelBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stay Here</title>
    <link rel="icon" href="icon.png" />
</head>
<body>
    <form id="form1" runat="server">
        <h1>Room Booking</h1>
        <fieldset>
            <legend>Personal Information</legend>
            <div>
                <asp:Label runat="server" Text="Title: " AssociatedControlID="title"></asp:Label>
                <!-- AssociatedControlID used to focus on target when we click on label and helpful for disable people -->
                <asp:DropDownList runat="server" ID="title">
                    <asp:ListItem Text="Mr." Value="Mr"></asp:ListItem>
                    <asp:ListItem Text="Ms." Value="Ms"></asp:ListItem>
                    <asp:ListItem Text="Mrs." Value="Mrs"></asp:ListItem>
                </asp:DropDownList>

                <asp:Label runat="server" Text="First Name*: " AssociatedControlID="clientFirstName" ID="clientFirstNameLabel"></asp:Label> 
                <asp:TextBox runat="server" ID="clientFirstName" placeholder="First Name" CausesValidation="true" ></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter First Name!!" ControlToValidate="clientFirstName" ID="validatorFirstName"></asp:RequiredFieldValidator>
            
                <asp:Label runat="server" Text="Last Name*: " ID="clientLastNameLabel" AssociatedControlID="clientLastName"></asp:Label>
                <asp:TextBox runat="server" ID="clientLastName" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Last Name!!" ControlToValidate="clientLastName" ID="validatorLastName" ></asp:RequiredFieldValidator>
        </div>
        </fieldset>
        <br />
            
            
        <div>
            <asp:Label runat="server" Text="Email*: " ID="clientEmaillabel" AssociatedControlID="clientEmail"></asp:Label>
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email" ></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="validatorEmail" ControlToValidate="clientEmail" ErrorMessage="Invalid Email format!!!!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="validatorFieldEmail"  runat="server" ControlToValidate="clientEmail" ErrorMessage="Enter Email.."></asp:RequiredFieldValidator>
        </div>
        <br />
        <div>
            <asp:Label runat="server" Text="Confirm Email*: " ID="confirmClientEmailLabel" AssociatedControlID="confirmClientEmail"></asp:Label>
            <asp:TextBox runat="server" ID="confirmClientEmail" placeholder="Email"></asp:TextBox>
            <asp:CompareValidator runat="server" ID="compareEmail" ErrorMessage="Email do no match" ControlToCompare="clientEmail" ControlToValidate="confirmClientEmail"></asp:CompareValidator>
            <asp:RegularExpressionValidator runat="server" ID="validatorConfirmClientEmail" ControlToValidate="confirmClientEmail" ErrorMessage="Invalid Email format!!!!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="validatorConfirmFieldEmail"  runat="server" ControlToValidate="confirmClientEmail" ErrorMessage="Enter Email.."></asp:RequiredFieldValidator>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="contactLabel" Text="Contact No*: " AssociatedControlID="clientContact"></asp:Label>
            <asp:TextBox runat="server" ID="clientContact" placeholder="Contact No"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientContact" ErrorMessage="Please enter contact no"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" ErrorMessage="Enter Valid No.!!" ControlToValidate="clientContact"></asp:RegularExpressionValidator>
         </div><!-- RegularExpressionValidator used to check perticular expression -->
        <br />

        <div>
            <asp:Label runat="server" Text="Suite Type " AssociatedControlID="suiteType"></asp:Label>
                <asp:DropDownList runat="server" ID="suiteType">
                    <asp:ListItem Text="Two room King Suite" Value="king suite"></asp:ListItem>
                    <asp:ListItem Text="Deluxe" Value="Deluxe"></asp:ListItem>
                    <asp:ListItem Text="Single Bed" Value="Single Bed"></asp:ListItem>
                </asp:DropDownList>
        </div>
        <br />
         <div>
            <asp:Label ID="foodFacilityLabel" Text="Food Facility :  " runat="server"></asp:Label>
             <div id="foodFacilityCheckbox" runat="server">
                <asp:CheckBox runat="server" ID="breakfast" Text="Breakfast" />
                <asp:CheckBox runat="server" ID="lunch" Text="Lunch" />
                <asp:CheckBox runat="server" ID="dinner" Text="Dinner" />
             </div>
        </div>
        <br />
        <div>
            <fieldset>
                <legend>Duration</legend>
                <div>
                    <asp:Label Text="Start" runat="server" AssociatedControlID="startDate"></asp:Label>
                     <asp:TextBox ID="startDate" runat="server" Text='<%# Bind("startDate","{0:dd-mm-yyyy}") %>' TextMode="Date"></asp:TextBox>
                     <asp:Label Text="End" runat="server" AssociatedControlID="endDate"></asp:Label>
                    <asp:TextBox ID="endDate" runat="server" Text='<%# Bind("endDate","{0:yyyy-mm-dd}") %>' TextMode="Date"></asp:TextBox>

                    <asp:Label Text="Stay days" AssociatedControlID="stayDays" runat="server"></asp:Label>
                     <asp:DropDownList runat="server" ID="stayDays">
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                    </asp:DropDownList>
                    
        
                </div>
            </fieldset>
        </div>
        <div>
            <asp:Label runat="server" ID="paymentModeLabel" Text="Mode of Payment*: " AssociatedControlID="paymentMode"></asp:Label>
            <asp:RadioButtonList runat="server" ID="paymentMode">
                <asp:ListItem runat="server" Text="Debit card" GroupName="paymentMode" />
                <asp:ListItem runat="server" Text="Online banking" GroupName="paymentMode"/>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ID="validatorfielPaymentMode" ControlToValidate="paymentMode" ErrorMessage="Please select Payment mode"></asp:RequiredFieldValidator>
        </div>
        <br />
        <div>
            <asp:Button Text="Submit" runat="server" ID="submitButton" OnClick="Book"/>
        </div>
        <br />
        <!--------------------- To Display Booking details---------------->
        <asp:Label ID="orderDetails" runat="server"><strong></strong></asp:Label>
        <div id="bookingDetails" runat="server"></div>

      <!-- <asp:ValidationSummary runat="server" ID="validSum" HeaderText="Mandetory Fields" />-->
    </form>
</body>
</html>
