﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Room
    {
        //PUblic Scope fields can be accessed outside the class also.
        public string suitType;
        public List<string> foodFacility;
        //public DateTime startDate;
        public string startDate;
        //public DateTime endDate;
        public string endDate;
        public int stayDays;
        
        
        //Parameterized Constructor to initialize fields of Room class.
       public  Room(string suitType, List<string> foodFacility, string startDate, string endDate, int stayDays)
        {
            this.suitType = suitType;
            this.foodFacility = foodFacility;
            this.startDate = startDate;
            this.endDate = endDate;
            this.stayDays = stayDays;
        }
    }
}