﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Client
    {
        private string clientTitle;
        private string clientLName;
        private string clientFName;
        private string clientEmail;
        private string clientContact;

       public Client()//DEFAULT CONSTRUCTOR
        {
            clientTitle = "Mr";
            clientFName = "XYZ";
            clientLName = "ABC";
            clientEmail = "abc@gmail.com";
            clientContact = "1234567892";
        }
        //GETTERS AND SETTERS FOR CLIENT CLASS
        public string Title
        {
            get{return clientTitle;}
            set { clientTitle = value; }
        }
        public string ClientFName
        {
            get { return clientFName; }
            set { clientFName = value; }
        }

        public string ClientLName
        {
            get { return clientLName; }
            set { clientLName = value; }
        }
        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
        public string ClientContact
        {
            get { return clientContact; }
            set { clientContact = value; }
        }
    }
}