﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Booking    //CLASS TO COMBINE OTHER THREE CLASSES
    {
        public Client client;
        public Room room;
        public  Payment payment;

        public Booking(Client client, Room room, Payment payment)
        {
            this.client = client;
            this.room = room;
            this.payment = payment;

        }

        public string BookingDetails()//OUTPUT METHOD
        {
             string receipt = "<br>";
           
            receipt += "Name: "+client.Title+" "+client.ClientFName+" "+client.ClientLName+"<br/>";
            receipt += "Email: " + client.ClientEmail + "<br/>";
            receipt += "Phone Number: " + client.ClientContact + "<br/>";

            receipt += "Booking For: "+ room.suitType+" Room.<br/>";
            receipt += "Facility Required: " + string.Join(", ",room.foodFacility.ToArray() ) + "<br/>";
            receipt += "Staying Period:- From " +room.startDate +" To:-"+room.endDate+"<br/>";
            receipt += "Your total is :$" + CalculateBill().ToString() +" for "+room.stayDays+" days.<br/>";
            receipt += "Payment Mode : " + payment.PaymentMode ;
            
            return receipt;
        }

        public double CalculateBill()//METHOD TO CALCULATE BILL
        {
            double total = 0;
            double tax = 0.13;
            double king = 500;
            double deluxe = 400;
            double single = 200;
            if(room.suitType=="King Suite")
            {
                total = (king*room.stayDays*tax)+ king * room.stayDays;
                return total;
            }
            else if(room.suitType=="deluxe")
            {
                total = (deluxe*room.stayDays* tax)+ deluxe * room.stayDays;
                return total;
            }
            else
            {
                total = (single*room.stayDays*tax)+ single * room.stayDays;
                return total;
            }

        }
       
    }
}