﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Payment
    {
        //THIS CLASS IS MADE TO STORE PAYMENT MODE AND PAYMENT ID IN FUTURE
        private string paymentMode;
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }
    }
}